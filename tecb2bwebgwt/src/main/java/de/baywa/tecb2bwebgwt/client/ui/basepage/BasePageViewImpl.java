package de.baywa.tecb2bwebgwt.client.ui.basepage;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.gwtplatform.mvp.client.ViewImpl;

import javax.inject.Inject;

import de.knightsoftnet.navigation.client.ui.basepage.AbstractBasePagePresenter;

/**
 * View of the CLOP BasePage.
 *
 * @author Manfred Tremmel
 * @version $Rev$, $Date$
 *
 */
public class BasePageViewImpl extends ViewImpl implements BasePagePresenter.MyView {

  /**
   * view interface.
   */
  interface BasePageViewUiBinder extends UiBinder<Widget, BasePageViewImpl> {
  }

  @UiField
  SimplePanel header;

  @UiField
  SimplePanel navigation;

  @UiField
  SimplePanel footer;

  @UiField
  SimplePanel container;

  /**
   * constructor injecting fields.
   *
   * @param pUiBinder ui binder of the page
   */
  @Inject
  public BasePageViewImpl(final BasePageViewUiBinder pUiBinder) {
    super();

    initWidget(pUiBinder.createAndBindUi(this));

    bindSlot(BasePagePresenter.SLOT_HEADER, header);
    bindSlot(BasePagePresenter.SLOT_NAVIGATION, navigation);
    bindSlot(BasePagePresenter.SLOT_FOOTER, footer);
    bindSlot(AbstractBasePagePresenter.SLOT_MAIN_CONTENT, container);
  }
}
