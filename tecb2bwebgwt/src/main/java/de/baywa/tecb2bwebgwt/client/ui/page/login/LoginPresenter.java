package de.baywa.tecb2bwebgwt.client.ui.page.login;

import de.knightsoftnet.gwtp.spring.client.rest.helper.AbstractPresenterWithErrorHandling;
import de.knightsoftnet.gwtp.spring.client.rest.helper.EditorWithErrorHandling;
import de.knightsoftnet.gwtp.spring.client.rest.helper.RestCallbackBuilder;
import de.knightsoftnet.gwtp.spring.client.session.Session;
import de.knightsoftnet.navigation.client.ui.basepage.AbstractBasePagePresenter;
import de.knightsoftnet.validators.client.event.FormSubmitHandler;

import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.rest.delegates.client.ResourceDelegate;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.NoGatekeeper;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;

import de.baywa.tecb2bwebgwt.client.services.LoginLogoutRestService;
import de.baywa.tecb2bwebgwt.client.ui.navigation.NameTokens;
import de.baywa.tecb2bwebgwt.shared.dto.LoginDto;

import javax.inject.Inject;

/**
 * Activity/Presenter of the login, implementation.
 *
 * @author Manfred Tremmel
 * @version $Rev$, $Date$
 *
 */
public class LoginPresenter extends
    AbstractPresenterWithErrorHandling<LoginPresenter.MyProxy, LoginPresenter.MyView, LoginDto> {

  public interface MyView
      extends EditorWithErrorHandling<LoginPresenter, LoginDto>, FormSubmitHandler<LoginDto> {
  }

  @ProxyStandard
  @NameToken(NameTokens.LOGIN)
  @NoGatekeeper
  public interface MyProxy extends ProxyPlace<LoginPresenter> {
  }

  /**
   * loginData data to remember.
   */
  private final LoginDto loginData;
  private final ResourceDelegate<LoginLogoutRestService> service;
  private final Session session;

  /**
   * constructor injecting parameters.
   *
   * @param peventBus event bus
   * @param pview page view
   * @param pproxy page proxy
   * @param pservice remote user service
   * @param psession session data
   */
  @Inject
  public LoginPresenter(final EventBus peventBus, final LoginPresenter.MyView pview,
      final MyProxy pproxy, final ResourceDelegate<LoginLogoutRestService> pservice,
      final Session psession) {
    super(peventBus, pview, pproxy, AbstractBasePagePresenter.SLOT_MAIN_CONTENT);
    service = pservice;
    session = psession;
    loginData = new LoginDto();
    getView().setPresenter(this);
    getView().fillForm(loginData);
  }

  /**
   * try to login with form data.
   */
  public final void tryToLogin() {
    service.withCallback(RestCallbackBuilder.buildLoginCallback(getView(), session))
        .login(loginData);
  }
}
