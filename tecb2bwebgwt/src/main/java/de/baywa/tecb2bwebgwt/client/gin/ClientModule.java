package de.baywa.tecb2bwebgwt.client.gin;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;
import com.gwtplatform.mvp.client.gin.DefaultModule;

import de.baywa.tecb2bwebgwt.client.ui.navigation.NameTokens;

public class ClientModule extends AbstractPresenterModule {

  @Override
  protected void configure() {
    install(new DefaultModule.Builder().defaultPlace(NameTokens.OVERVIEW)
        .errorPlace(NameTokens.LOGIN).unauthorizedPlace(NameTokens.LOGIN).build());

    install(new ApplicationModule());
    install(new DispatchModule());
  }
}
