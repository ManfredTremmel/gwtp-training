package de.baywa.tecb2bwebgwt.client.ui.basepage;

import de.baywa.tecb2bwebgwt.client.ui.basepage.footer.BasePageFooterPresenter;
import de.baywa.tecb2bwebgwt.client.ui.basepage.header.BasePageHeaderPresenter;

import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NoGatekeeper;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.presenter.slots.PermanentSlot;
import com.gwtplatform.mvp.client.proxy.Proxy;

import de.knightsoftnet.navigation.client.ui.basepage.AbstractBasePagePresenter;
import de.knightsoftnet.navigation.client.ui.navigation.NavigationPresenter;

/**
 * Activity/Presenter of the base page, implementation.
 *
 * @author Manfred Tremmel
 * @version $Rev$, $Date$
 *
 */
public class BasePagePresenter
    extends AbstractBasePagePresenter<BasePagePresenter.MyView, BasePagePresenter.MyProxy> {

  public interface MyView extends View {
  }

  @ProxyStandard
  @NoGatekeeper
  public interface MyProxy extends Proxy<BasePagePresenter> {
  }

  private final BasePageHeaderPresenter basePageHeaderPresenter;
  private final NavigationPresenter navigationPresenter;
  private final BasePageFooterPresenter basePageFooterPresenter;

  public static final PermanentSlot<BasePageHeaderPresenter> SLOT_HEADER = new PermanentSlot<>();
  public static final PermanentSlot<NavigationPresenter> SLOT_NAVIGATION = new PermanentSlot<>();
  public static final PermanentSlot<BasePageFooterPresenter> SLOT_FOOTER = new PermanentSlot<>();

  /**
   * constructor injecting parameters.
   *
   * @param pEventBus event bus
   * @param pView view of the page
   * @param pProxy proxy of the page
   * @param pBasePageHeaderPresenter presenter of the base page header
   * @param pNavigationPresenter presenter of the navigation
   * @param pBasePageFooterPresenter presenter of the base page footer
   */
  @Inject
  public BasePagePresenter(final EventBus pEventBus, final BasePagePresenter.MyView pView,
      final MyProxy pProxy, final BasePageHeaderPresenter pBasePageHeaderPresenter,
      final NavigationPresenter pNavigationPresenter,
      final BasePageFooterPresenter pBasePageFooterPresenter) {
    super(pEventBus, pView, pProxy);
    basePageHeaderPresenter = pBasePageHeaderPresenter;
    navigationPresenter = pNavigationPresenter;
    basePageFooterPresenter = pBasePageFooterPresenter;
  }


  @Override
  protected void onBind() {
    this.setInSlot(BasePagePresenter.SLOT_HEADER, basePageHeaderPresenter);
    this.setInSlot(BasePagePresenter.SLOT_NAVIGATION, navigationPresenter);
    this.setInSlot(BasePagePresenter.SLOT_FOOTER, basePageFooterPresenter);
  }
}
