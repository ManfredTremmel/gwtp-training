package de.baywa.tecb2bwebgwt.client.ui.page.logout;

import de.knightsoftnet.gwtp.spring.client.session.Session;
import de.knightsoftnet.navigation.client.gatekeepers.LoggedInGatekeeper;
import de.knightsoftnet.navigation.client.ui.basepage.AbstractBasePagePresenter;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.rest.delegates.client.ResourceDelegate;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.NoGatekeeper;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.annotations.UseGatekeeper;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;

import de.baywa.tecb2bwebgwt.client.services.LoginLogoutRestService;
import de.baywa.tecb2bwebgwt.client.ui.navigation.NameTokens;

import javax.inject.Inject;

/**
 * Activity/Presenter of the logout, implementation.
 *
 * @author Manfred Tremmel
 * @version $Rev$, $Date$
 *
 */
public class LogoutPresenter extends Presenter<LogoutPresenter.MyView, LogoutPresenter.MyProxy> {

  public interface MyView extends View {
  }

  @ProxyStandard
  @NameToken(NameTokens.LOGOUT)
  @NoGatekeeper
  @UseGatekeeper(LoggedInGatekeeper.class)
  public interface MyProxy extends ProxyPlace<LogoutPresenter> {
  }

  private final ResourceDelegate<LoginLogoutRestService> service;
  private final Session session;

  /**
   * constructor injecting parameters.
   *
   * @param peventBus event bus
   * @param pview view of the page
   * @param pproxy proxy of the page
   * @param pservice user remote service
   * @param psession session data
   */
  @Inject
  public LogoutPresenter(final EventBus peventBus, final LogoutPresenter.MyView pview,
      final MyProxy pproxy, final ResourceDelegate<LoginLogoutRestService> pservice,
      final Session psession) {
    super(peventBus, pview, pproxy, AbstractBasePagePresenter.SLOT_MAIN_CONTENT);
    service = pservice;
    session = psession;
  }

  @Override
  protected void onReveal() {
    super.onReveal();
    service.withCallback(new AsyncCallback<Void>() {

      @Override
      public void onFailure(final Throwable pcaught) {
        // TODO Auto-generated method stub
      }

      @Override
      public void onSuccess(final Void presult) {
        // TODO Auto-generated method stub
      }
    }).logout();
    session.setUser(null);
  }
}
