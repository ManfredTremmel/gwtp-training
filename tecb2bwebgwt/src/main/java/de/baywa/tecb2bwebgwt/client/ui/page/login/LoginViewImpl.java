package de.baywa.tecb2bwebgwt.client.ui.page.login;

import de.knightsoftnet.gwtp.spring.client.rest.helper.AbstractViewWithErrorHandling;
import de.knightsoftnet.mtwidgets.client.ui.widget.PasswordTextBox;
import de.knightsoftnet.mtwidgets.client.ui.widget.TextBox;
import de.knightsoftnet.validators.client.editor.BeanValidationEditorDriver;
import de.knightsoftnet.validators.client.editor.annotation.IsValidationDriver;
import de.knightsoftnet.validators.client.event.FormSubmitEvent;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTMLPanel;

import de.baywa.tecb2bwebgwt.shared.dto.LoginDto;

import javax.inject.Inject;

public class LoginViewImpl extends AbstractViewWithErrorHandling<LoginPresenter, LoginDto>
    implements LoginPresenter.MyView {

  @UiField
  TextBox inpUser;

  @UiField
  PasswordTextBox inpPwd;

  @UiField
  Button btLogin;

  interface LoginViewImplUiBinder extends UiBinder<HTMLPanel, LoginViewImpl> {
  }

  @IsValidationDriver
  public interface Driver extends BeanValidationEditorDriver<LoginDto, LoginViewImpl> {
  }

  @Inject
  public LoginViewImpl(final Driver pdriver, final LoginViewImplUiBinder puiBinder) {
    super(pdriver);
    initWidget(puiBinder.createAndBindUi(this));
    driver.initialize(this);
    driver.setSubmitButton(btLogin);
    driver.addFormSubmitHandler(this);
  }

  @Override
  public final void showMessage(final String pmessage) {
    GWT.log(pmessage);
  }

  @Override
  public void onFormSubmit(final FormSubmitEvent<LoginDto> pevent) {
    presenter.tryToLogin();
  }
}
