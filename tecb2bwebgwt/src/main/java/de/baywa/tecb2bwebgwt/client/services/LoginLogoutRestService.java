package de.baywa.tecb2bwebgwt.client.services;

import de.baywa.tecb2bwebgwt.shared.dto.LoginDto;
import de.baywa.tecb2bwebgwt.shared.dto.UserDto;

import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

@Path("login")
public interface LoginLogoutRestService {

  @POST
  UserDto login(final LoginDto ploginDto);

  @DELETE
  void logout();
}
