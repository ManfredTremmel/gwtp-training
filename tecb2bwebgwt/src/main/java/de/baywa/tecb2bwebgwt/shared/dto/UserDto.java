package de.baywa.tecb2bwebgwt.shared.dto;

import de.knightsoftnet.gwtp.spring.shared.models.User;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

public class UserDto implements Serializable, User {
  private static final long serialVersionUID = 6580037177274322029L;

  private String user;
  private String firstName;
  private String lastName;

  public String getUser() {
    return user;
  }

  public void setUser(final String puser) {
    user = puser;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(final String pfirstName) {
    firstName = pfirstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(final String plastName) {
    lastName = plastName;
  }

  @Override
  public void setUserName(final String puserName) {
    user = puserName;
  }

  @Override
  public String getUserName() {
    return user;
  }

  @Override
  public boolean isLoggedIn() {
    return StringUtils.isNotEmpty(user);
  }
}
